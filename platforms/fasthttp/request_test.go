package jsonrouter

import (
	"testing"
	//
	"github.com/valyala/fasthttp"
	//
	"gitlab.com/TheDarkula/jsonrouter/platforms"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

func TestFasthttpRequest(t *testing.T) {

	ctx := &fasthttp.RequestCtx{}
	req := NewRequestObject(&tree.Node{}, ctx)

	platforms.RunStandardTests(t, req)

}
