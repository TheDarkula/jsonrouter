package jsonrouter

import (
	"strconv"
	//
	"github.com/valyala/fasthttp"
	//
	"gitlab.com/TheDarkula/jsonrouter"
	"gitlab.com/TheDarkula/jsonrouter/logging"
	"gitlab.com/TheDarkula/jsonrouter/metrics"
	"gitlab.com/TheDarkula/jsonrouter/platforms"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

type FastHttpRouter func(ctx *fasthttp.RequestCtx)

// Serve is a function which calls the ListenAndServe func in the fasthttp package.
func (router FastHttpRouter) Serve(port int, maxRequestBodySize ...int) error {

	var bodySize int = 30000000
	if len(maxRequestBodySize) > 0 {
		bodySize = maxRequestBodySize[0]
	}

	s := &fasthttp.Server{
		Handler: fasthttp.RequestHandler(router),
		MaxRequestBodySize: bodySize,
	}
	return s.ListenAndServe(":"+strconv.Itoa(port))
}

// Serve is a function which calls the ListenAndServeTLS func in the fasthttp package.
func (router FastHttpRouter) ServeTLS(port int, crt, key string, maxRequestBodySize ...int) error {

	var bodySize int = 30000000
	if len(maxRequestBodySize) > 0 {
		bodySize = maxRequestBodySize[0]
	}

	s := &fasthttp.Server{
		Handler: fasthttp.RequestHandler(router),
		MaxRequestBodySize: bodySize,
	}

	return s.ListenAndServeTLS(":"+strconv.Itoa(port), crt, key)
}

// New creates a JSONrouter for the fasthttp platform.
func New(serviceName string, logger logging.Logger, spec interface{}) (*tree.Node, FastHttpRouter) {

	config := &tree.Config{
		Spec:       spec,
		Log:        logger,
		ServiceName: serviceName,
		Metrics:    metrics.NewMetrics(),
		MetResults: map[string]interface{}{},
	}
	root := tree.NewNode(config)

	platforms.AddMetricsEndpoints(root)
	platforms.AddSpecEndpoints(root)

	return root, FastHttpRouter(
		func(ctx *fasthttp.RequestCtx) {

			core.MainHandler(
				NewRequestObject(root, ctx),
				root,
				string(ctx.Path()),
			)

		},
	)
}
