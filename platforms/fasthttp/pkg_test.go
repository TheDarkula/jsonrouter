package jsonrouter

import (
	"testing"
	"time"
	//
	"gitlab.com/TheDarkula/jsonrouter/logging/testing"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
)

const (
	CONST_HOPEFULLYUNUSEDPORT = 3543
)

func TestServe(t *testing.T) {

	_, router := New(
		"serviceName",
		logs.NewClient().NewLogger(),
		openapiv2.New("localhost", "testing"),
	)

	failChan := make(chan error)

	go func() {
		if err := router.Serve(CONST_HOPEFULLYUNUSEDPORT); err != nil {
			failChan <- err
		} else {
			failChan <- nil
		}
	}()

	time.Sleep(2 * time.Second)

	select {

	case hasError := <-failChan:

		t.Error(hasError)

	default:

	}
}
