package platforms

import (
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
)

func RunStandardTests(t *testing.T, req http.Request) {

	StandardTests_headers(t, req)
	StandardTests_parameters(t, req)

}
