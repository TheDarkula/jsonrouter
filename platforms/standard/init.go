// Package jsonrouter implements the http-router for net/http projects
package jsonrouter

import (
	ht "net/http"
	//"fmt"
	"gitlab.com/TheDarkula/jsonrouter"
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/logging"
	"gitlab.com/TheDarkula/jsonrouter/metrics"
	"gitlab.com/TheDarkula/jsonrouter/openapi"
	"gitlab.com/TheDarkula/jsonrouter/platforms"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

// New creates a JSONrouter for the vanilla platform.
func New(log logging.Logger, spec interface{}) (*platforms.Router, error) {

	if err := openapi.ValidSpec(spec); err != nil {
		return nil, err
	}

	config := &tree.Config{
		Spec:       spec,
		Log:        log,
		Metrics:    metrics.NewMetrics(),
		MetResults: map[string]interface{}{},
	}

	root := tree.NewNode(config)

	platforms.AddMetricsEndpoints(root)
	platforms.AddSpecEndpoints(root)

	return platforms.NewRouter(
		root,
		func(res ht.ResponseWriter, r *ht.Request) {

			core.MainHandler(
				http.NewMockRequest("", ""),
				root,
				r.URL.Path,
			)

		},
	), nil
}
