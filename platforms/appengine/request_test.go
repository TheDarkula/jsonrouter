package jsonrouter

import (
	"bytes"
	"net/http/httptest"
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/platforms"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

func TestAppengineRequest(t *testing.T) {

	platforms.RunStandardTests(
		t,
		NewRequestObject(
			&tree.Node{},
			httptest.NewRecorder(),
			httptest.NewRequest("POST", "https://google.com", bytes.NewBuffer(nil)),
		),
	)
}
