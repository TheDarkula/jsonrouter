package jsonrouter

import (
	www "net/http"
	//
	"gitlab.com/TheDarkula/jsonrouter"
	"gitlab.com/TheDarkula/jsonrouter/metrics"
	"gitlab.com/TheDarkula/jsonrouter/platforms"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

// Creates a JSONrouter for App Engine platforms
func New(spec interface{}) (*platforms.Router, error) {

	config := &tree.Config{
		Spec:       spec,
		Metrics:    metrics.NewMetrics(),
		MetResults: map[string]interface{}{},
	}
	root := tree.NewNode(config)

	platforms.AddMetricsEndpoints(root)
	platforms.AddSpecEndpoints(root)

	return platforms.NewRouter(
		root,
		func(res www.ResponseWriter, r *www.Request) {

			core.MainHandler(
				NewRequestObject(root, res, r),
				root,
				r.URL.Path,
			)

		},
	), nil
}
