package openapi

import (
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/tree"
)

type Spec interface {
	Build(handlerMap map[string]*tree.Handler) *http.Status
}
