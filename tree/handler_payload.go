package tree

import (
	"fmt"
	"reflect"
	json "github.com/json-iterator/go"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

// ReadPayload validates any payload present in the request body, according to the payloadSchema
func (handler *Handler) ReadPayload(req http.Request) *http.Status {

	var paramCount int
	var optionalCount int
	bodyParams := map[string]interface{}{}
	statusMessages := map[string]*http.Status{}

	// handle payload
	switch params := handler.payloadSchema.(type) {

	case nil, []byte:

	// do nothing

	case map[string]interface{}, []interface{}, validation.Object:

		status := req.ReadBodyObject()
		if status != nil {
			return status
		}

	case validation.Array:

		switch len(params) {

		case 1:

			return req.Respond(400, "INVALID TYPE FOR ARRAY PAYLOAD SCHEMA, EXPECTS 0 OR 2 ARGS (*ValidationConfig, paramKey)")

		case 2:

			vc, ok := params[0].(*validation.Config)
			if !ok {
				return req.Respond(500, "INVALID ARRAY PAYLOAD SCHEMA VALIDATION CONFIG")
			}

			paramKey, ok := params[1].(string)
			if !ok {
				return req.Respond(500, "INVALID ARRAY PAYLOAD SCHEMA PARAM KEY")
			}

			status, params := vc.BodyFunction(req, req.BodyArray())
			if status != nil {

				req.Log().DebugJSON(req.BodyArray())
				//return req.Respond(400, "INVALID TYPE FOR ARRAY PAYLOAD ITEM, EXPECTED: "+vc.Type())

				return status
			}

			req.SetParam(paramKey, params)

		}

	case validation.Payload:

		status := req.ReadBodyObject()
		if status != nil {
			return status
		}

		pCount, oCount := parsePayload(req, params, bodyParams, statusMessages)
		paramCount += pCount
		optionalCount += oCount

	default:

		return req.Respond(500, "INVALID OPTIONAL PAYLOAD SCHEMA CONFIG TYPE: "+reflect.TypeOf(params).String())

	}

	if len(statusMessages) > 0 {
		b, _ := json.Marshal(statusMessages)
		for _, status := range statusMessages {
			return req.Respond(status.Code, string(b))
		}
	}

	lp := len(bodyParams)
	if len(bodyParams) < paramCount {
		return req.Respond(
			400,
			fmt.Sprintf(
				"INVALID PAYLOAD FIELD COUNT %v EXPECTED %v/%v",
				lp,
				paramCount,
				paramCount+optionalCount,
			),
		)
	}

	req.SetBodyParams(bodyParams)

	return nil
}

func parsePayload(req http.Request, object validation.Payload, bodyParams map[string]interface{}, statusMessages map[string]*http.Status) (paramCount, optionalCount int) {

	for key, vc := range object {

		status, x := vc.BodyFunction(
			req,
			req.Body(key),
		)

		if vc.RequiredValue {
			paramCount++
			if status != nil {
				// dont leak data to logs
				//status.Value = req.Body(key)
				status.Message = fmt.Sprintf("%s KEY '%s'", status.MessageString(), key)
				statusMessages[key] = status
				continue
			}
		} else {
			optionalCount++
			if status != nil {
				continue
			}
		}

		bodyParams[key] = x
	}

	return
}
