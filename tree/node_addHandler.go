package tree

// addHandler literally adds a handler to the node object under a http method
func (node *Node) addHandler(handler *Handler) {

	handler.UpdateSpec()

	node.Lock()
	defer node.Unlock()
	node.Methods[handler.Method] = handler
}
