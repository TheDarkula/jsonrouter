package tree

import (
	"fmt"
	json "github.com/json-iterator/go"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

// ReadPayload validates any payload present in the request body, according to the payloadSchema
func (handler *Handler) ReadHeaderPayload(req http.Request) *http.Status {

	var paramCount int
	var optionalCount int
	headerParams := map[string]interface{}{}
	statusMessages := map[string]*http.Status{}

	pCount, oCount := parseHeaderPayload(req, handler.requiredHeaders, headerParams, statusMessages)
	paramCount += pCount
	optionalCount += oCount

	if len(statusMessages) > 0 {
		b, err := json.Marshal(statusMessages)
		if err != nil {
			req.Log().NewError("PROBLEM MARSHALLING STATUS MESSAGES")
			return req.Fail()
		}
		for _, status := range statusMessages {
			return req.Respond(status.Code, string(b))
		}
	}

	lp := len(headerParams)
	if len(headerParams) < paramCount {
		return req.Respond(
			400,
			fmt.Sprintf(
				"INVALID PAYLOAD FIELD COUNT %v EXPECTED %v/%v",
				lp,
				paramCount,
				paramCount+optionalCount,
			),
		)
	}

	req.SetHeaderParams(headerParams)

	return nil
}

func parseHeaderPayload(req http.Request, object validation.Payload, bodyParams map[string]interface{}, statusMessages map[string]*http.Status) (paramCount, optionalCount int) {

	for key, vc := range object {

		status, x := vc.PathFunction(
			req,
			req.GetRequestHeader(key),
		)

		if vc.RequiredValue {
			paramCount++
			if status != nil {
				// dont leak data to logs
				//status.Value = req.Body(key)
				status.Message = fmt.Sprintf("%s KEY '%s'", status.MessageString(), key)
				statusMessages[key] = status
				continue
			}
		} else {
			optionalCount++
			if status != nil {
				continue
			}
		}

		bodyParams[key] = x
	}

	return
}
