package tree

import (
	"reflect"
	"strings"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v3"
)

// UpdateSpec pushed updates into the spec object
func (handler *Handler) UpdateSpec() *Handler {

	openapiSpec := handler.Node.Config.Spec

	switch spec := openapiSpec.(type) {

	default:
		panic("INVALID TYPE FOR THE PROVIDED SPEC: " + reflect.TypeOf(openapiSpec).String())

	case nil:
		panic("THE API SPEC OBJECT IS NIL!")

	case *openapiv2.Spec:

		if spec.Paths == nil {
			spec.Paths = make(map[string]openapiv2.Path)
		}

		pathMethod := &openapiv2.PathMethod{
			Produces: []string{"application/json"},
			Description: handler.Descr,
			Responses: map[int]*openapiv2.Response{},
		}

		// http 500 status
		s500 := pathMethod.Response(500)
		s500.Description = http.CONST_HTTP_STATUS_MSG_500

		if handler.Method != "GET" {
			// http 400 status
			s400 := pathMethod.Response(400)
			s400.Description = http.CONST_HTTP_STATUS_MSG_400
		}

		// http 200 status
		s200 := pathMethod.Response(200)
		s200.Description = http.CONST_HTTP_STATUS_MSG_200
		s200.Schema = &openapiv2.StatusSchema{
			Type: "object",
		}

		path := handler.Path(spec.BasePath)
		if spec.Paths[path] == nil {
			spec.Paths[path] = openapiv2.Path{}
		}
		spec.Paths[path][strings.ToLower(handler.Method)] = pathMethod

	case *openapiv3.Spec:

		if spec.Paths == nil {
			spec.Paths = make(map[string]openapiv3.Path)
		}

		operation := &openapiv3.Operation{
			Summary:     "",
			Description: handler.Descr,
			Parameters:  []*openapiv3.Parameter{},
			Responses: map[int]*openapiv3.Response{
				200: &openapiv3.Response{
					Description: "OK!",
				},
				500: &openapiv3.Response{
					Description: "Unknown Server Error",
				},
				400: &openapiv3.Response{
					Description: "Bad Request",
				},
			},
		}

		path := handler.Path("")
		if spec.Paths[path] == nil {
			spec.Paths[path] = openapiv3.Path{}
		}
		spec.Paths[path][strings.ToLower(handler.Method)] = operation

	}

	// update this handler's spec
	handler.updateSpecHeaders()
	handler.updateParameters()

	return handler
}
