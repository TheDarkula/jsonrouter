package tree

import (
	"strings"

	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
//	"gitlab.com/TheDarkula/jsonrouter/openapi/v3"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

// SetHeaders allows the headers for the HTTP response to be set by providing a map of header key, values.
func (handler *Handler) RequiredHeaders(headers ...validation.Payload) *Handler {

	for _, object := range headers {
		for k, v := range object {
			v.Required()
			handler.requiredHeaders[k] = v
		}
		//handler.updateSpecParams(true, object)
	}

	handler.updateSpecHeaders()

	return handler
}
// updateSpecHeaders triggers an update of the headers in the spec object
func (handler *Handler) updateSpecHeaders() {


	switch spec := handler.Node.Config.Spec.(type) {
	case *openapiv2.Spec:

		path := spec.Paths[handler.Path(spec.BasePath)]
		pathMethod := path[strings.ToLower(handler.Method)]

		x := 200
		for k, v := range handler.requiredHeaders {
			response := pathMethod.Response(x)
			response.Headers[k] = &openapiv2.Header{
				Type:    openapiv2.Type(v.Model),
				Default: k,
			}
		}

/*
	case *openapiv3.Spec:
			path := spec.Paths[handler.Path("")]
			operation := path[strings.ToLower(handler.Method)]

			x := 200
			for k, v := range handler.Headers {
				response := operation.Response(x)
				response.Headers[k] = &openapiv3.Header{
					Schema: &openapiv3.Schema{
						Type: openapiv3.Type(v),
						Default: fmt.Sprintf("%v", v),
					},
				}
			}
*/
	}
}
