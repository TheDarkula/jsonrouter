package tree

import (
	"fmt"
	"io/ioutil"
	"mime"
	www "net/http"
	"path"
	"strings"
	"sync"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v3"
	"gitlab.com/TheDarkula/jsonrouter/security"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

type HandlerFunction func(req http.Request) *http.Status

// newHandler initialises a handler instance
func (node *Node) newHandler(method string) *Handler {
	handler := &Handler{
		Node: node,
		Method: method,
		requiredHeaders: validation.Payload{},
		SecurityModule: node.SecurityModule,
	}
	return handler
}

type Handler struct {
	Node *Node
	Method string
	Descr string
	Function func(req http.Request) *http.Status
	File *File
	requiredHeaders validation.Payload
	responseSchema interface{}
	payloadSchema  interface{}
	patchSchema    []interface{}
	spec           struct {
		addedBodyDefinition bool
	}
	SecurityModule security.SecurityModule
	sync.RWMutex
}

func (handler *Handler) Path(removePrefix ...string) string {
	return strings.Replace(handler.Node.FullPath(), removePrefix[0], "", 1)
}

func (handler *Handler) Ref(basePath string) string {
	return strings.Replace(
		fmt.Sprintf(
			"%s-%s",
			handler.Path(basePath),
			handler.Method,
		),
		"/",
		"+",
		-1,
	)
}

func (handler *Handler) Security(sec security.SecurityModule) *Handler {
	handler.SecurityModule = sec
	switch spec := handler.Node.Config.Spec.(type) {
	case *openapiv2.Spec:
		spec.SecurityDefinitions[sec.Name()] = sec.DefinitionV2()
	case *openapiv3.Spec:
		//spec.SecurityDefinitions[sec.Name()] = sec.DefinitionV3()
	default:
		panic("INVALID SWITCH VALUE")
	}

	return handler
}

func (handler *Handler) DetectContentType(req http.Request, filePath string) *http.Status {

	if handler.File.Cache == nil || !handler.Node.Config.CacheFiles {

		// handle potential trailing slash on folder path declaration
		filePath := strings.Replace(filePath, "//", "/", -1)

		b, err := ioutil.ReadFile(filePath)
		if err != nil {
			return req.Respond(404, err.Error())
		}

		handler.File.Cache = b
		handler.File.MimeType = mime.TypeByExtension(path.Ext(filePath))
		if handler.File.MimeType == "" {
			handler.File.MimeType = www.DetectContentType(b)
		}
	}

	return nil
}

// Description describes the function via the spec JSON
func (handler *Handler) Description(descr string) *Handler {

	handler.Descr = descr

	return handler.UpdateSpec()
}

// Response applies model which describes response schema
func (handler *Handler) Response(schema ...interface{}) *Handler {

	handler.responseSchema = schema[0]

	return handler
}

func (handler *Handler) UseFunction(f interface{}) {

	switch v := f.(type) {

	case func(http.Request) *http.Status:

		handler.Function = v

	default:

		panic("INVALID ARGUMENT TYPE FOR HANDLER METHOD FUNCTION DECLARATION")

	}
}
