package tree

import (
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

func TestParsePayload1(t *testing.T) {

	req := http.NewMockRequest("", "")

	handler := (&Node{}).newHandler("POST")
	handler.Required(
		validation.Payload{
			"hello1": validation.String(1, 10),
			"bye2":   validation.String(1, 10),
		},
	)
	handler.Optional(
		validation.Payload{
			"hello": validation.String(1, 10),
			"bye":   validation.String(1, 10),
		},
	)

	req.SetBodyObject(
		map[string]interface{}{
			"hello": "fuck off",
		},
	)

	statusses := map[string]*http.Status{}
	bodyParams := map[string]interface{}{}

	pCount, oCount := parsePayload(req, handler.payloadSchema.(validation.Payload), bodyParams, statusses)

	if oCount != 2 {
		t.Fatalf("OCOUNT IS WRONG %d", oCount)
	}

	if pCount != 2 {
		t.Fatalf("PCOUNT IS WRONG %d", pCount)
	}

	if len(statusses) != 2 {
		t.Fatalf("ERRORS ARE WRONG %d", len(statusses))
	}

}

func TestParsePayload2(t *testing.T) {

	req := http.NewMockRequest("", "")

	handler := (&Node{}).newHandler("POST")
	handler.Required(
		validation.Payload{
			"hello1": validation.String(1, 10),
			"bye2":   validation.String(1, 10),
			"bye3":   validation.String(1, 10),
		},
	)
	handler.Optional(
		validation.Payload{
			"hello": validation.String(1, 10),
			"bye":   validation.String(1, 10),
		},
	)

	req.SetBodyObject(
		map[string]interface{}{
			"hello": "fuck off",
			"bye3":  "fucdfsdhjvfisdhfgisdgk off",
		},
	)

	statusses := map[string]*http.Status{}
	bodyParams := map[string]interface{}{}

	pCount, oCount := parsePayload(req, handler.payloadSchema.(validation.Payload), bodyParams, statusses)

	if oCount != 2 {
		t.Fatalf("OCOUNT IS WRONG %d", oCount)
	}

	if pCount != 3 {
		t.Fatalf("PCOUNT IS WRONG %d", pCount)
	}

	if len(statusses) != 3 {
		t.Fatalf("ERRORS ARE WRONG %d", len(statusses))
	}

}

func TestParsePayload3(t *testing.T) {

	req := http.NewMockRequest("", "")

	handler := (&Node{}).newHandler("POST")
	handler.Required(
		validation.Payload{},
	)
	handler.Optional(
		validation.Payload{},
	)

	req.SetBodyObject(
		map[string]interface{}{
			"hello": "fuck off",
			"bye3":  "fucdfsdhjvfisdhfgisdgk off",
		},
	)

	statusses := map[string]*http.Status{}
	bodyParams := map[string]interface{}{}

	pCount, oCount := parsePayload(req, handler.payloadSchema.(validation.Payload), bodyParams, statusses)

	if oCount != 0 {
		t.Fatalf("OCOUNT IS WRONG %d", oCount)
	}

	if pCount != 0 {
		t.Fatalf("PCOUNT IS WRONG %d", pCount)
	}

	if len(statusses) != 0 {
		t.Fatalf("ERRORS ARE WRONG %d", len(statusses))
	}

}
