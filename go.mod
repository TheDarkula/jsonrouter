module gitlab.com/TheDarkula/jsonrouter

go 1.12

require (
	cloud.google.com/go v0.44.3
	cloud.google.com/go/bigquery v1.0.1 // indirect
	cloud.google.com/go/datastore v1.0.0
	cloud.google.com/go/logging v1.0.0
	github.com/dghubble/sling v1.3.0 // indirect
	github.com/fatih/color v1.7.0
	github.com/fatih/structs v1.1.0 // indirect
	github.com/golangdaddy/go.uuid v2.0.0+incompatible
	github.com/golangdaddy/tarantula v0.0.0-20190104150304-3a6b306edcd3
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/hjmodha/goDevice v0.0.0-20170127191456-6bec48aa958b
	github.com/json-iterator/go v1.1.7
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/ttacon/builder v0.0.0-20170518171403-c099f663e1c2 // indirect
	github.com/ttacon/libphonenumber v1.0.1
	github.com/valyala/fasthttp v1.4.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	golang.org/x/tools v0.0.0-20190816200558-6889da9d5479 // indirect
	google.golang.org/appengine v1.6.1
	google.golang.org/genproto v0.0.0-20190817000702-55e96fffbd48 // indirect
	google.golang.org/grpc v1.23.0 // indirect
	gopkg.in/resty.v1 v1.12.0
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
