package generate

import (
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/dashboard/gen"
	"gitlab.com/TheDarkula/jsonrouter/logging/testing"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/platforms/standard"
)

func TestMain(t *testing.T) {

	Generate()

	router, err := jsonrouter.New(
		logs.NewClient().NewLogger(),
		openapiv2.New("localhost", "test"),
	)
	if err != nil {
		t.Fail()
	}

	staticContent := static.New()
	staticContent.Dashboard(router.Node)

}
