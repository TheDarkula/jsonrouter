package tests

import (
	"testing"

	"gitlab.com/TheDarkula/jsonrouter/platforms/fasthttp"
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/logging/testing"
	"gitlab.com/TheDarkula/jsonrouter"
	"gitlab.com/TheDarkula/jsonrouter/validation"
)

const (
	CONST_SERVICE_NAME = "a service"
)

func TestServe(t *testing.T) {

	// create router
	root, _ := jsonrouter.New(
		CONST_SERVICE_NAME,
		logs.NewClient().NewLogger(CONST_SERVICE_NAME),
		openapiv2.New(CONST_SERVICE_NAME, "a service"),
	)

	root.SetHeaders(
		map[string]interface{}{
			"Access-Control-Allow-Headers":	"Authorization,Content-Type",
			"Access-Control-Allow-Origin": "*",
		},
	)

	fullpath := "/api/test"

	req := http.NewMockRequest("POST", fullpath)

	req.SetRequestHeader("z", "3")
	req.SetRequestHeader("x", "1")
	req.SetRequestHeader("y", "2")

	root.Add("/api").Add("/test").POST(
		func (http.Request) *http.Status {

			req.Log().DebugJSON(req.HeaderParams())


			z, ok := req.HeaderParam("z").(int)
			if !ok {
				req.Log().NewError("FAILED TO GET z")
				return req.Fail()
			}
			req.Log().NewErrorf("Z is %d", z)

			x, ok := req.HeaderParam("x").(int)
			if !ok {
				req.Log().NewError("FAILED TO GET X")
				return req.Fail()
			}
			req.Log().NewErrorf("X is %d", x)

			y, ok := req.HeaderParam("y").(int)
			if !ok {
				req.Log().NewError("FAILED TO GET y")
				return req.Fail()
			}
			req.Log().NewErrorf("Y is %d", y)

			if x != 1 || y != 2  || z != 3{
				req.Log().Debugf("INVALID HEADER VALUES %d %d", x, y)
				return req.Fail()
			}

			return nil
		},
	).RequiredHeaders(
		validation.Payload{
			"x": validation.Int(),
			"y": validation.Int(),
			"z": validation.Int(),
		},
	)

	status := core.MainHandler(req, root, fullpath)
	if status.Code != 200 {
		req.Log().DebugJSON(status)
		t.Fail()
	}

}
