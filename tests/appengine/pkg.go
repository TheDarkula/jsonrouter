package appenginetest

import (
	"fmt"
	ht "net/http"
	"testing"
	"time"
	//
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/platforms/appengine"
	"gitlab.com/TheDarkula/jsonrouter/tree"
	//
	"gitlab.com/TheDarkula/jsonrouter/tests/common"
)

func TestServer(t *testing.T, node *tree.Node) *common.TestHTTPStruct {

	s := openapiv2.New(common.CONST_SPEC_HOST, common.CONST_SPEC_TITLE)
	s.BasePath = common.CONST_SPEC_BASEPATH
	s.Info.Contact.URL = common.CONST_SPEC_URL
	s.Info.Contact.Email = common.CONST_SPEC_EMAIL
	s.Info.License.URL = common.CONST_SPEC_URL

	service, err := jsonrouter.New(s)
	if err != nil {
		t.Error(err)
		t.Fail()
		return nil
	}
	service.Node.Config.RecordMetrics()

	self := &common.TestHTTPStruct{
		T:    t,
		Met:  &service.Node.Config.Metrics,
		Port: common.CONST_PORT_APPENGINE,
	}

	// make the supplied routing work on this root node
	if node != nil {
		service.Node.Use(node)
	}

	go func() {
		panic(
			ht.ListenAndServe(
				fmt.Sprintf(
					":%d",
					common.CONST_PORT_APPENGINE,
				),
				service,
			),
		)
	}()

	// wait for router to be serving
	time.Sleep(time.Second / 10)

	return self
}
