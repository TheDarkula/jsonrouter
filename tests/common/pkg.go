package common

import (
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/metrics"
)

type TestHTTPStruct struct {
	T    *testing.T
	Met  *metrics.Metrics
	Port int
}

func dummyHandler(req http.Request) *http.Status {

	return nil
}
