package fasthttptest

import (
	"testing"
	"time"
	//
	"gitlab.com/TheDarkula/jsonrouter/logging/testing"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	"gitlab.com/TheDarkula/jsonrouter/platforms/fasthttp"
	"gitlab.com/TheDarkula/jsonrouter/tree"
	//
	"gitlab.com/TheDarkula/jsonrouter/tests/common"
)

func TestServer(t *testing.T, node *tree.Node) *common.TestHTTPStruct {

	s := openapiv2.New(common.CONST_SPEC_HOST, common.CONST_SPEC_TITLE)
	s.BasePath = common.CONST_SPEC_BASEPATH
	s.Info.Contact.URL = common.CONST_SPEC_URL
	s.Info.Contact.Email = common.CONST_SPEC_EMAIL
	s.Info.License.URL = common.CONST_SPEC_URL

	log := logs.NewClient().NewLogger()

	root, service := jsonrouter.New("serviceName", log, s)
	root.Config.RecordMetrics()

	self := &common.TestHTTPStruct{
		T:    t,
		Met:  &root.Config.Metrics,
		Port: common.CONST_PORT_FASTHTTP,
	}

	// make the supplied routing work on this root node
	if node != nil {
		root.Use(node)
	}

	go func() {
		panic(
			service.Serve(common.CONST_PORT_FASTHTTP),
		)
	}()

	// wait for router to be serving
	time.Sleep(time.Second / 10)

	return self
}
