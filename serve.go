package core

import (
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/tree"
	"strconv"
	"strings"
	//"gitlab.com/TheDarkula/jsonrouter/metrics"
	//"fmt"
)

const (
	ROBOTS_TXT = "User-agent: *\nDisallow: /api/"
)

type Router interface {
	Serve(int)
}

type Headers map[string]string

// MainHandler is the main handler function for incoming requests
func MainHandler(req http.Request, node *tree.Node, fullPath string) (status *http.Status) {

	// enforce https-only if required
	if node.Config.ForcedTLS {
		if !req.IsTLS() {
			status = req.Respond(502, "PLEASE UPGRADE TO HTTPS")
			return
		}
	}

	if node.Config.UseMetrics {
		met := node.Config.Metrics
		met.Timers["requestTime"].Start()
		defer func() {

			if status == nil {
				status = req.Respond(200, "OK")
			} else {
				status.Respond(req)
			}

			met.MultiCounters["requestMethods"].Increment(req.Method())
			met.MultiCounters["requestMethods"].Update(met.SetResults)

			met.MultiCounters["responseCodes"].Increment(strconv.Itoa(status.Code))
			met.MultiCounters["responseCodes"].Update(met.SetResults)

			met.Counters["requestCount"].Increment()
			met.Counters["requestCount"].Update(met.SetResults)

			met.Timers["requestTime"].Stop()
			met.Timers["requestTime"].Update(met.SetResults)
		}()
	} else {
		defer func() {

			if status == nil {
				status = req.Respond(200, "OK")
			} else {
				status.Respond(req)
			}

		}()
	}

	switch fullPath {

	case "/robots.txt":
		status = req.Respond([]byte(ROBOTS_TXT))
		return

	}

	segments := strings.Split(fullPath, "/")[1:]

	next := node

	for _, segment := range segments {

		for k, v := range next.GetHeaders() {
			req.SetResponseHeader(k, v.(string))
		}

		if len(segment) == 0 {
			break
		}

		var n *tree.Node
		n, status = next.Next(req, segment)
		if status != nil {
			return
		}

		if n != nil {
			next = n
			continue
		}

		req.Respond()

		if req.Method() != "OPTIONS" {
			status = req.Respond(404, node.Config.ServiceName + ": NO ROUTE FOUND")
			return
		}

	}

	handler := next.Handler(req)

	if req.Method() != "OPTIONS" {
		// resolve handler
		if handler == nil {
			status = req.Respond(404, node.Config.ServiceName + ": CONTROLLER NOT FOUND")
			return
		}
	} else {
		// return if preflight request
		status = req.Respond(200, "OK")
		return
	}

	// read the request headers and unmarshal into specified schema
	status = handler.ReadHeaderPayload(req)
	if status != nil {
		return
	}

	// read the request body and unmarshal into specified schema
	status = handler.ReadPayload(req)
	if status != nil {
		return
	}

	// execute modules
	status = handler.Node.RunModules(req)
	if status != nil {
		return
	}

	if handler.File != nil {

		status = handler.DetectContentType(req, handler.File.Path)
		if status != nil {
			return
		}

		req.SetResponseHeader("Content-Type", handler.File.MimeType)

		status = req.Respond(handler.File.Cache)
		return
	}

	if handler.Function == nil {
		req.Log().Panic("FAILED TO GET FUNCTION TO SERVE HANDLE OPERATION!")
	}

	// execute the handler
	status = handler.Function(req)
	return
}
