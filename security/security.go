package security

import (
	"gitlab.com/TheDarkula/jsonrouter/http"
	"gitlab.com/TheDarkula/jsonrouter/openapi/v2"
	//	"gitlab.com/TheDarkula/jsonrouter/openapi/v3"
)

type SecurityModule interface {
	Name() string
	Validate(http.Request) *http.Status
	DefinitionV2() *openapiv2.SecurityDefinition
	//	DefinitionV3() *openapiv3.SecurityDefinition
}
