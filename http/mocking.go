package http

import (
	"io"
	"net/http"
	"net/url"
	"os"
	"sync"
	//
	"github.com/golangdaddy/go.uuid"
	"gitlab.com/TheDarkula/jsonrouter/logging"

	"gitlab.com/TheDarkula/jsonrouter/logging/testing"
	"gitlab.com/TheDarkula/jsonrouter/platforms/parameters"
)

// NewMockRequest creates an implementation of the Request interface for testing or other.
func NewMockRequest(method, path string) Request {
	return &MockRequest{
		Parameters: parameters.New(),
		method: method,
		path: path,
		device: "Mobile",
		bodyObject: map[string]interface{}{},
		bodyArray: []interface{}{},
		requestHeaders: map[string]string{},
		responseHeaders: map[string]string{},
		log: logs.NewClient().NewLogger("MockRequest"),
	}
}

type MockRequest struct {
	*parameters.Parameters
	method          string
	device          string
	path            string
	params          map[string]interface{}
	bodyParams      map[string]interface{}
	bodyObject		map[string]interface{}
	bodyArray		[]interface{}
	requestHeaders  map[string]string
	responseHeaders map[string]string
	log             logging.Logger
	sync.RWMutex
}

// UID returns a UUID that has been generated randoomly for this request.
func (self *MockRequest) UID() (string, error) {

	uid, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	return uid.String(), nil
}

// Testing
func (req *MockRequest) Testing() bool {
	return true
}

// FullPath
func (self *MockRequest) FullPath() string {
	return self.path
}

// IsTls
func (self *MockRequest) IsTLS() bool {
	return false
}

// Method
func (self *MockRequest) Method() string {
	return self.method
}

// Device
func (self *MockRequest) Device() string {
	return self.method
}

// Body
func (self *MockRequest) Body(s string) interface{} {
	self.RLock()
	defer self.RUnlock()
	return 0
}

// SetParam sets a value from the params object.
func (self *MockRequest) SetParam(k string, v interface{}) {
	self.Lock()
	defer self.Unlock()
	self.params[k] = v
}

// SetParam replaces the params object with the supplied map.
func (self *MockRequest) SetParams(m map[string]interface{}) {
	self.Lock()
	defer self.Unlock()
	self.params = m
}

// Param
func (self *MockRequest) Param(k string) interface{} {
	self.RLock()
	defer self.RUnlock()
	return self.params[k]
}

// Params
func (self *MockRequest) Params() map[string]interface{} {
	self.RLock()
	defer self.RUnlock()
	return self.params
}

// SetRequestHeader
func (self *MockRequest) SetRequestHeader(k, v string) {
	self.Lock()
	defer self.Unlock()
	self.requestHeaders[k] = v
}

// GetRequestHeader
func (self *MockRequest) GetRequestHeader(k string) string {
	self.RLock()
	defer self.RUnlock()
	return self.requestHeaders[k]
}

// SetResponseHeader
func (self *MockRequest) SetResponseHeader(k, v string) {
	self.Lock()
	defer self.Unlock()
	self.responseHeaders[k] = v
}

// GetResponseHeader
func (self *MockRequest) GetResponseHeader(k string) string {
	self.RLock()
	defer self.RUnlock()
	return self.responseHeaders[k]
}

// RawBody
func (self *MockRequest) RawBody() (*Status, []byte) {
	self.RLock()
	defer self.RUnlock()
	return nil, []byte{}
}

// ReadBodyObject
func (self *MockRequest) ReadBodyObject() *Status {
	self.RLock()
	defer self.RUnlock()
	return nil
}

// ReadBodyArray
func (self *MockRequest) ReadBodyArray() *Status {
	self.RLock()
	defer self.RUnlock()
	return nil
}

// BodyObject
func (self *MockRequest) BodyObject() map[string]interface{} {
	self.RLock()
	defer self.RUnlock()
	return self.bodyObject
}

// BodyArray
func (self *MockRequest) BodyArray() []interface{} {
	self.RLock()
	defer self.RUnlock()
	return self.bodyArray
}

// SetBodyObject
func (self *MockRequest) SetBodyObject(obj map[string]interface{}) {
	self.Lock()
	defer self.Unlock()
	self.bodyObject = obj
}

// SetBodyArray
func (self *MockRequest) SetBodyArray(array []interface{}) {
	self.Lock()
	defer self.Unlock()
	self.bodyArray = array
}

// Redirect
func (self *MockRequest) Redirect(s string, x int) *Status {
	return nil
}

// ServeFile
func (self *MockRequest) ServeFile(s string) {

}

// HttrError
func (self *MockRequest) HttpError(s string, x int) {

}

// Writer
func (self *MockRequest) Writer() io.Writer {
	return &rW{}
}

// WriteString
func (self *MockRequest) WriteString(s string) (int, error) {
	return len(s), nil
}

// Write
func (self *MockRequest) Write(b []byte) (int, error) {
	return 0, nil
}

// Fail
func (self *MockRequest) Fail() *Status {
	return Fail()
}

// Respond
func (self *MockRequest) Respond(args ...interface{}) *Status {
	status, contentType := Respond(args...)
	self.SetResponseHeader("Content-Type", contentType)
	return status
}

// Log
func (self *MockRequest) Log() logging.Logger {
	return self.log
}

// Res
func (self *MockRequest) Res() http.ResponseWriter {
	return &rW{}
}

// R
func (self *MockRequest) R() interface{} {
	return &http.Request{
		URL: &url.URL{
			Path: self.path,
		},
	}
}

type rW struct {
	status int
	size   int
	http.ResponseWriter
}

// Status
func (w *rW) Status() int {
	return w.status
}

// Size
func (w *rW) Size() int {
	return w.size
}

// Header
func (w *rW) Header() http.Header {
	return w.ResponseWriter.Header()
}

// Write
func (w *rW) Write(data []byte) (int, error) {

	written, err := os.Stdin.Write(data)
	w.size += written

	return written, err
}

// WriteHeader
func (w *rW) WriteHeader(statusCode int) {

	w.status = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}
