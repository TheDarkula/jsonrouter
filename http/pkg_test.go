package http

import (
	"testing"
)

func TestMockRequest(t *testing.T) {

	email := "address@example.com"

	mockRequest := NewMockRequest("", "")
	mockRequest.SetParams(
		map[string]interface{}{
			"email": email,
		},
	)
	if mockRequest.Params() == nil {
		t.Fatal("PARAMS NOT WORKING")
	}
	if _, ok := mockRequest.Param("email").(string); !ok {
		t.Fatal("CANT FIND FIELD")
	}
}
