package benchmarks

import (
	"fmt"
	"testing"
	//
	"gitlab.com/TheDarkula/jsonrouter/tests/common"
	"gopkg.in/resty.v1"
	//
	"gitlab.com/TheDarkula/jsonrouter/benchmarks/standard"
)

func init() {
	go server.Start()
}

func BenchmarkStandardGET(b *testing.B) {

	url := fmt.Sprintf("http://localhost:%d/endpoint/0", common.CONST_PORT_STANDARD)
	//fmt.Println(url)

	// Actual benchmark starts here
	for n := 0; n < b.N; n++ {
		resp, err := resty.R().Get(url)
		if err != nil {
			b.Error(err)
			return
		}
		if resp.StatusCode() != 200 {
			b.Error(resp.Status())
			return
		}
	}
}

func BenchmarkStandardPOST(b *testing.B) {

	url := fmt.Sprintf("http://localhost:%d/endpoint/0", common.CONST_PORT_STANDARD)
	//fmt.Println(url)

	payload := map[string]interface{}{
		"hello": "world",
	}

	// Actual benchmark starts here
	for n := 0; n < b.N; n++ {
		resp, err := resty.R().SetBody(payload).Post(url)
		if err != nil {
			b.Error(err)
			return
		}
		if resp.StatusCode() != 200 {
			b.Error(resp.Status())
			return
		}
	}
}
